def display_board():
    print(  board[1] + '\t|' + board[2] + '\t|' + board[3])
    print("---------------")
    print(  board[4] + '\t|' + board[5] + '\t|' + board[6])
    print("---------------")
    print(board[7] + '\t|' + board[8] + '\t|' + board[9])
    print("--------------")


def player_input(mark):
    while True:
        inp = input(f"[player] '{mark}' Enter your choice:")
        if inp.isdigit() and int(inp) < 10 and int(inp) > 0:
            inp = int(inp)
            if board[inp] == " ":
                return inp
            else:
                print(f"[player] '{mark}' place already taken.")
        else:
            print(f"[player] '{mark}' Enter valid option (1 - 9).")


def winning(mark, board):
    winning_place = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], [2, 5, 8], [3, 6, 9], [1, 5, 9], [3, 5, 7]]
    for win_place in winning_place:
        if board[win_place[0]] == board[win_place[1]] == board[win_place[2]] == mark:
            return True


def win_move(i, board, mark):
    temp_board = list(board)
    temp_board[i] = mark
    if winning(mark, temp_board):
        return True
    else:
        return False


def comp_input(comp, player, board):
    for i in range(1, 10):
        if board[i] == ' ' and win_move(i, board, comp):
            return i
    for i in range(1, 10):
        if board[i] == ' ' and win_move(i, board, player):
            return i
    for i in [5, 1, 7, 3, 2, 9, 8, 6, 4]:
        if board[i] == ' ':
            return i


def new_game():
    while True:
        nxt = input('[player] Do you want to play again?(y/n):')
        if nxt in ['y', 'Y']:
            again = True
            break
        elif nxt in ['n', 'N']:
            again = False
            break
        else:
            print('Enter correct input')
    if again:
        print('___NEW GAME___')
        main_game()
    else:
        return False


def win_check(player, comp):
    winning_place = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], [2, 5, 8], [3, 6, 9], [1, 5, 9], [3, 5, 7]]
    for win_place in winning_place:
        if board[win_place[0]] == board[win_place[1]] == board[win_place[2]] == player:
            print('[player] wins the match!')
            status="player win"
            if not new_game():
                return False
        elif board[win_place[0]] == board[win_place[1]] == board[win_place[2]] == comp:
            print('[comp] wins the match!')
            status="comp win"
            if not new_game():
                return False
    if ' ' not in board:
        print('MATCH DRAW!!')
        status="tie"
        if not new_game():
            return False
    return True


def user_choice():
    while True:
        inp = input('[player]Choose your mark[x/o]: ')
        if inp in ['x', 'X']:
            print('[player]You choose "X".\n[player]You play first.')
            return 'x', 'o'
        elif inp in ['O', 'o']:
            print('[player] You choose "O".\n[player] comp plays first.')
            return 'o', 'x'
        else:
            print('[player] Enter correct input!')

print('Lets play TIC TAC TOE')
list=[]
i = 1
d={}
global board
play = True
board = ['', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
player, comp = user_choice()
display_board()
while play:
    if player == 'x':
        x = player_input(player)
        board[x] = player
        display_board()
        play = win_check(player, comp)
        if play:
                o = comp_input(comp, player, board)
                print(f'[comp] Entered:{o}')
                board[o] = comp
                display_board()
                play = win_check(player, comp)
    else:
            x = comp_input(comp, player, board)
            print(f'[comp] Entered:{x}')
            board[x] = comp
            display_board()
            play = win_check(player, comp)
            if play:
                o = player_input(player)
                board[o] = player
                display_board()
                play = win_check(player, comp)

list = [board, status]
d[i] = list
i = i+ 1
h=input("\nWant to check history? y/n")
while(h=='y'):
    i= int(input("round"))
    print("status")
    bo=d[i][0]
    s=d[i][1]
    display_board(bo)
    print(s)
    h=input("\nDo you want to continue checking? y/n")


